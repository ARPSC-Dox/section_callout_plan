# Michigan Section Callout Procedure

This document outlines the process for activating the Michigan
Section in the event of an incident.  Included are contact information,
frequencies, monitoring schedules, etc. needed in case communications
is disrupted.
